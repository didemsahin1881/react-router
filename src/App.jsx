
import {BrowserRouter, Route, RouterProvider, Routes, createBrowserRouter, createRoutesFromElements} from "react-router-dom"
import "./App.css";
import HomePage from "./pages/Home";
import ProductsPage from "./pages/Products";
import RouteLayout from "./pages/Root";
import MainNavigation from "./components/MainNavigation";
import ErrorPage from "./pages/Error";
import ProductDetailPage from "./pages/ProductDetail";
import RootLayout from "./pages/Root";

// const routeDefinitions = createRoutesFromElements([
//   <Route key="home" path="/" element={<HomePage />} />,
//   <Route key="products" path="/products" element={<ProductsPage />} />
// ]);
 const router = createBrowserRouter([
  {path: "/",
   element:<RouteLayout></RouteLayout>,
   errorElement:<ErrorPage></ErrorPage>,
children:[
  {index:true, element:<HomePage></HomePage>  }, 
  {path: "products", element:<ProductsPage></ProductsPage>  }, 
  {path: "products/:productId", element:<ProductDetailPage></ProductDetailPage>  }, 
]
}, 
   
 ])
//const router= createBrowserRouter(routeDefinitions);

function App() {
  return (
    <> 
      <BrowserRouter>
      <Routes>
        <Route
        path="/"
        element={<RootLayout></RootLayout>}
        >

<Route path="/" element={<HomePage></HomePage>}></Route>
        <Route path="/products" element={<ProductsPage></ProductsPage>}></Route>
        <Route path="/products/:productId" element={<ProductDetailPage></ProductDetailPage>}></Route>


        </Route>
      <Route path="*" element={<ErrorPage></ErrorPage>}></Route>
      </Routes>   
      </BrowserRouter>
    </>
  );
}

export default App;
