import React, { Fragment } from 'react'
import MainNavigation from '../components/MainNavigation'

const ErrorPage = () => {
  return (
    <Fragment>
        <MainNavigation></MainNavigation>
        <main>
        <h1>Bir Hata Oluştu</h1>
        <p>Bu sayfa bulunamadı</p>
    </main> 
    </Fragment>
  )
}

export default ErrorPage