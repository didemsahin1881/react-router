import { Fragment } from "react"
import { Link, useNavigate } from "react-router-dom"

const HomePage = () => {
  const navigate = useNavigate();
  function navigateHandler(){
    navigate("products");
  }
  return ( 
  <Fragment>
      <h1>My Home PAge</h1>
      {/* <a href='/products'>Go to Product Page</a> */}
      {/* <Link to='/products'> Go to product page</Link> */}
      <button onClick={navigateHandler}>Go to product page</button>
  </Fragment>
  )
}

export default HomePage