import React, { Fragment } from 'react'
import { Link, useParams } from 'react-router-dom'
const ProductDetailPage = () => {
    const params = useParams();
    console.log(params);
  return (
   <Fragment>
     <div>ProductDetail</div>
     <p>{params.productId}</p>
     <Link  to=".." relative='path'>Back</Link>
   </Fragment>
  )
}

export default ProductDetailPage