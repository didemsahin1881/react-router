import React, { Fragment } from 'react'
import { Outlet } from 'react-router-dom'
import MainNavigation from '../components/MainNavigation'

const RootLayout = () => {
  return (
<Fragment>
    <MainNavigation></MainNavigation>
    <div> Roout Layout</div>
    <Outlet></Outlet>
</Fragment>
  )
}

export default RootLayout